
DECLARE exp_start_date STRING DEFAULT '2022-04-19';
DECLARE exp_end_date STRING DEFAULT '2022-05-08'; 
--exp_end_date can be the current date as well while monitoring the result mid-exp. Note that making decision mid-exp (peeking) is not advisable. 


-- EXPORT DATA OPTIONS( uri='gs://core-search-dev-personalization/shweta/experiment_framework/exp_data*.parquet',
-- format='parquet', overwrite=TRUE) AS

DROP TABLE IF EXISTS `core-search-dev.sandbox_shweta.exp_day_level_dataset`; 
CREATE TABLE IF NOT EXISTS `core-search-dev.sandbox_shweta.exp_day_level_dataset` AS
WITH get_date_range as(
    SELECT 
    bucket,
    visitor_id, 
    login_status, 
    os_type,
    bucketed_at,
    DATE(exp_start_date) AS exp_start_date, 
    DATE(exp_end_date) AS exp_end_date, 
    day
    FROM `core-search-dev.sandbox_shweta.bucket_assignment` 
    CROSS JOIN (SELECT * FROM UNNEST(GENERATE_DATE_ARRAY(DATE(exp_start_date), DATE(exp_end_date), INTERVAL 1 DAY)) AS day) 
),

attach_tables as(
    SELECT 
    --visitor assignment
    bucket,
    t1.visitor_id, 
    login_status, 
    t1.os_type,
    bucketed_at,
    exp_start_date, 
    exp_end_date,
    t1.day as event_date,
    --widget activities
    COALESCE(widget_impression, 0) widget_impression,
    COALESCE(widget_impression_prod_list, []) widget_impression_prod_list,
    COALESCE(widget_click, 0) widget_click,
    COALESCE(widget_click_prod_list, []) widget_click_prod_list,
    COALESCE(widget_cartadd, 0) widget_cartadd,
    COALESCE(widget_cartadd_prod_list, []) widget_cartadd_prod_list,
    COALESCE(widget_order, 0) widget_order,
    COALESCE(widget_order_prod_list, []) widget_order_prod_list,
    COALESCE(widget_quantity, 0) widget_quantity,
    COALESCE(widget_revenue, 0) widget_revenue,
    --clicks
    COALESCE(total_click, 0) total_click,
    COALESCE(total_click_prod_list, []) total_click_prod_list,
    --cartadds
    COALESCE(total_cartadd, 0) total_cartadd,
    COALESCE(total_cartadd_prod_list, []) total_cartadd_prod_list,
    --order
    COALESCE(total_order, 0) total_order,
    COALESCE(total_order_prod_list, []) total_order_prod_list,
    COALESCE(total_quantity, 0) total_quantity,
    COALESCE(total_revenue, 0) total_revenue,
    --other activities
    COALESCE(session_visit_product_click_count, 0) session_visit_product_click_count,
    COALESCE(session_visit_product_click_list, []) session_visit_product_click_list,
    COALESCE(plp_visit_product_click_count, 0) plp_visit_product_click_count,
    COALESCE(plp_visit_product_click_list, []) plp_visit_product_click_list,
    COALESCE(slp_visit_product_click_count, 0) slp_visit_product_click_count,
    COALESCE(slp_visit_product_click_list, []) slp_visit_product_click_list

    from get_date_range t1
    left join `core-search-dev.sandbox_shweta.widget_data_read` t2
    on (t1.os_type=t2.os_type AND t1.visitor_id=t2.visitor_id AND t1.day=t2.day)
    left join `core-search-dev.sandbox_shweta.orders` t3
    on (t1.visitor_id=t3.visitor_id AND t1.day=t3.day)
    left join `core-search-dev.sandbox_shweta.cartadds` t4
    on (t1.os_type=t4.os_type AND t1.visitor_id=t4.visitor_id AND t1.day=t4.day)
    left join `core-search-dev.sandbox_shweta.clicks` t5
    on (t1.os_type=t5.os_type AND t1.visitor_id=t5.visitor_id AND t1.day=t5.day)
    left join `core-search-dev.sandbox_shweta.other_activity` t6
    on (t1.visitor_id=t6.visitor_id AND t1.day=t6.day)
)

select * from attach_tables