DECLARE start_date STRING DEFAULT '2022-04-19';
DECLARE end_date STRING DEFAULT '2022-05-08';


DROP TABLE IF EXISTS `core-search-dev.sandbox_shweta.cartadds`;
CREATE TABLE IF NOT EXISTS `core-search-dev.sandbox_shweta.cartadds` AS 

with get_all_cartadds as(
  SELECT 
  visitor_id, 
  DATE(date_time) as day,
  product_list,
  SPLIT(product_list, ';')[OFFSET(1)] as raw_product_id,
  COALESCE(t2.product_id, SPLIT(product_list, ';')[OFFSET(1)]) as product_id,
  CASE WHEN LOWER(user_agent) like '%android%' THEN 'Android' ELSE 'iOS' end as os_type
  FROM `tc-sc-bi-bigdata-dtl-fcom-prd.acc_fcm_cl_mktg_weba_prd.btd_clickstream` t1
  LEFT JOIN `falabella-search-prod.relevancy_matrices.facl_product_variant_mapping` t2
  ON (SPLIT(product_list, ';')[OFFSET(1)]=t2.variant_id) 
  WHERE DATE(_PARTITIONTIME) BETWEEN DATE(start_date) AND DATE(end_date)
  AND (LOWER(user_agent) LIKE '%android%' OR LOWER(user_agent) LIKE '%iphone%') 
  AND app_id is not null
  AND page_name='scadd' 
  -- AND page_name_app in ('scadd', 'product detail page') 
)

SELECT 
  os_type,
  visitor_id, 
  day, 
  COUNT(DISTINCT product_id) as total_cartadd,
  ARRAY_AGG(DISTINCT product_id IGNORE NULLS) as total_cartadd_prod_list
FROM get_all_cartadds
GROUP BY 1,2,3
HAVING total_cartadd>0



-- old query:
-- CREATE TEMP FUNCTION EXTRACT_PRODUCT_ID(page_url STRING, product_list STRING) AS (
-- CASE 
-- WHEN page_url like '%/product/%'
-- THEN SPLIT(SPLIT(page_url, '/product/')[OFFSET(1)], '/')[OFFSET(0)]
-- WHEN ((page_url like '%/category/%') OR (page_url like'%/search%')) AND ARRAY_LENGTH(SPLIT(product_list, ';'))>1 
-- THEN IF(SPLIT(product_list, ';')[OFFSET(1)]='undefined', Null, SPLIT(product_list, ';')[OFFSET(1)]) END   
-- );


-- DROP TABLE IF EXISTS `core-search-dev.sandbox_shweta.cartadds`;
-- CREATE TABLE IF NOT EXISTS `core-search-dev.sandbox_shweta.cartadds` AS 
-- with get_all_cartadds as(
--   SELECT 
--   concat(post_visid_high, post_visid_low) as visitor_id, 
--   hash_email_id,
--   day,
--   EXTRACT_PRODUCT_ID(page_url, product_list) as product_id
--   FROM `falabella-search-prod.relevancy_matrices.facl_raw_omniture` 
--   WHERE day BETWEEN DATE(start_date) AND DATE(end_date)
--   AND lower(event_data) like '%addtocart%'
-- )

-- SELECT 
--   visitor_id, 
--   hash_email_id,
--   day, 
--   COUNT(DISTINCT product_id) as total_cartadd,
--   ARRAY_AGG(DISTINCT product_id IGNORE NULLS) as total_cartadd_prod_list
-- FROM get_all_cartadds
-- GROUP BY 1,2,3
-- HAVING total_cartadd>0

