
-- columns needed: visitor_id, login_status, bucket_assignment, os_type
-- NOTE: This is test script for bucket assignment, we need to have generic code for bucket assignment

DECLARE exp_start_date STRING DEFAULT '2022-04-19';
DECLARE exp_end_date STRING DEFAULT '2022-05-08'; 
DECLARE widgets ARRAY<STRING> DEFAULT ['Más_opciones_similares|3', 'Varias_personas_después_miran|3;Más_opciones_similares|3', 
'Varias_personas_después_miran|3'];

DROP TABLE IF EXISTS `core-search-dev.sandbox_shweta.bucket_assignment`;
CREATE TABLE IF NOT EXISTS `core-search-dev.sandbox_shweta.bucket_assignment` AS 

WITH trt AS(
SELECT DISTINCT 
    CONCAT(post_visid_high, post_visid_low) as visitor_id,
    CASE WHEN LOWER(user_agent) like '%iphone%' THEN 'iOS' ELSE 'Android' END AS os_type,
    IF(hash_email_id is not null, 'logged_in', 'logged_out') login_status,
    DATE(exp_start_date) AS bucketed_at, --this should ideally be the date when the user enters the exp, not all the users in the exp enters on start date of the exp
    row_number() over (partition by CONCAT(post_visid_high, post_visid_low) order by day asc) as r
FROM `falabella-search-prod.relevancy_matrices.facl_raw_omniture`
WHERE
DATE(_PARTITIONTIME) BETWEEN DATE(exp_start_date) AND DATE(exp_end_date) 
-- need to parametrize these exp specific filters
AND ((LOWER(user_agent) like '%iphone%' AND split(coalesce(mobileappid,post_mobileappid),' ')[safe_offset(1)]>'2.2.16') OR 
     (LOWER(user_agent) like '%android%' AND split(coalesce(mobileappid,post_mobileappid),' ')[safe_offset(1)]>'2.3.0'))
AND widget_display in UNNEST(widgets)
),


cntrl AS(
SELECT DISTINCT 
    CONCAT(post_visid_high, post_visid_low) as visitor_id,
    CASE WHEN LOWER(user_agent) like '%iphone%' THEN 'iOS' ELSE 'Android' END AS os_type,
    IF(hash_email_id is not null, 'logged_in', 'logged_out') login_status,
    DATE(exp_start_date) AS bucketed_at, --this should ideally be the date when the user enters the exp, not all the users in the exp enters on start date of the exp
    row_number() over (partition by CONCAT(post_visid_high, post_visid_low) order by day asc) as r
FROM `falabella-search-prod.relevancy_matrices.facl_raw_omniture`
LEFT JOIN (
  SELECT DISTINCT
  concat(visit_high, visit_low) as visitor_id
  FROM `falabella-search-prod.recommendation.facl_widget_impression`
  WHERE source='App'
  AND day BETWEEN DATE(exp_start_date) AND DATE(exp_end_date) 
) p2
ON (CONCAT(post_visid_high, post_visid_low) = p2.visitor_id)
LEFT JOIN trt ON (CONCAT(post_visid_high, post_visid_low)=trt.visitor_id)
WHERE p2.visitor_id IS NULL AND trt.visitor_id IS NULL
AND DATE(_PARTITIONTIME) BETWEEN DATE(exp_start_date) AND DATE(exp_end_date) 
AND ((LOWER(user_agent) like '%iphone%' AND split(coalesce(mobileappid,post_mobileappid),' ')[safe_offset(1)]>'2.2.16') OR 
     (LOWER(user_agent) like '%android%' AND split(coalesce(mobileappid,post_mobileappid),' ')[safe_offset(1)]>'2.3.0'))
),

merge_all as(
  (select visitor_id, os_type, login_status, 'treatment' as bucket, bucketed_at FROM trt WHERE r=1)
  UNION ALL
  (select visitor_id, os_type, login_status, 'control' as bucket, bucketed_at FROM cntrl WHERE r=1)
)

select visitor_id, login_status, bucket, os_type, bucketed_at from merge_all
