
DECLARE start_date STRING DEFAULT '2022-04-19';
DECLARE end_date STRING DEFAULT '2022-05-08'; 
DECLARE widget_ids STRING DEFAULT '(más_opciones_similares|varias_personas_después_miran|varias personas después miran|más opciones similares)';


DROP TABLE IF EXISTS `core-search-dev.sandbox_shweta.widget_data_read`;
CREATE TABLE IF NOT EXISTS `core-search-dev.sandbox_shweta.widget_data_read` AS 
with get_impressions_raw as(
  select os_type,
  concat(visit_high, visit_low) as visitor_id,
  day,
  t1.product_id as raw_product_id,
  COALESCE(t2.product_id, t1.product_id) as product_id
  FROM `falabella-search-prod.recommendation.facl_widget_impression` t1
  LEFT JOIN `falabella-search-prod.relevancy_matrices.facl_product_variant_mapping` t2
  ON (t1.product_id=t2.variant_id) 
  WHERE source='App'
  AND REGEXP_CONTAINS(widget_id, widget_ids)
  AND day BETWEEN DATE(start_date) AND DATE(end_date)
),

get_impressions as(
  select os_type,
  visitor_id,
  day,
  COUNT(DISTINCT product_id) as widget_impression,
  ARRAY_AGG(DISTINCT product_id) as widget_impression_prod_list
  FROM get_impressions_raw
  GROUP BY 1,2,3
),

get_clicks_raw as(
  select DISTINCT
  os_type,
  concat(visit_high, visit_low) as visitor_id,
  day,
  t1.product_id as raw_product_id,
  COALESCE(t2.product_id, t1.product_id) as product_id
  FROM `falabella-search-prod.recommendation.facl_widget_click` t1
  LEFT JOIN `falabella-search-prod.relevancy_matrices.facl_product_variant_mapping` t2
  ON (t1.product_id=t2.variant_id) 
  WHERE source='App'
  AND REGEXP_CONTAINS(widget_id, widget_ids)
  AND day BETWEEN DATE(start_date) AND DATE(end_date)
),

get_clicks as(
  select 
  os_type,
  visitor_id,
  day,
  COUNT(DISTINCT product_id) as widget_click,
  ARRAY_AGG(DISTINCT product_id) as widget_click_prod_list
  FROM get_clicks_raw
  GROUP BY 1,2,3
),

get_cartadds_raw as(
  select 
  os_type,
  concat(visit_high, visit_low) as visitor_id,
  day,
  t1.product_id as raw_product_id,
  COALESCE(t2.product_id, t1.product_id) as product_id
  FROM `falabella-search-prod.recommendation.facl_widget_cartadd` t1
  LEFT JOIN `falabella-search-prod.relevancy_matrices.facl_product_variant_mapping` t2
  ON (t1.product_id=t2.variant_id) 
  WHERE source='App'
  AND REGEXP_CONTAINS(widget_id, widget_ids)
  AND day BETWEEN DATE(start_date) AND DATE(end_date)
),

get_cartadds as(
  select 
  t1.os_type,
  t1.visitor_id,
  t1.day,
  COUNT(DISTINCT t1.product_id) as widget_cartadd,
  ARRAY_AGG(DISTINCT t1.product_id) as widget_cartadd_prod_list
  FROM get_cartadds_raw t1
  INNER JOIN get_clicks_raw t2
  ON (t1.visitor_id=t2.visitor_id and t1.day=t2.day and t1.product_id=t2.product_id)
  GROUP BY 1,2,3
),

get_widget_orders as(
  SELECT
  DISTINCT
  os_type,
  concat(visit_high, visit_low) as visitor_id,
  day,
  t1.product_id as raw_product_id,
  COALESCE(t2.product_id, t1.product_id) as product_id
  FROM `falabella-search-prod.recommendation.facl_widget_orders` t1
  LEFT JOIN `falabella-search-prod.relevancy_matrices.facl_product_variant_mapping` t2
  ON (t1.product_id=t2.variant_id) 
  WHERE source='App'
  AND REGEXP_CONTAINS(widget_id, widget_ids)
  AND day BETWEEN DATE(start_date) AND DATE(end_date)
),

get_raw_orders as(
  SELECT  
  CONCAT(post_visid_high, post_visid_low) as visitor_id,
  day,
  t1.product_id as raw_product_id,
  COALESCE(t2.product_id, t1.product_id) as product_id,
  quantity,
  price
  FROM `falabella-search-prod.recommendation.facl_omniture_orders` t1
  LEFT JOIN `falabella-search-prod.relevancy_matrices.facl_product_variant_mapping` t2
  on (t1.product_id=t2.variant_id)
  WHERE day BETWEEN DATE(start_date) and DATE(end_date)
),

get_orders as(
  SELECT 
  t1.visitor_id,
  t1.day,
  COUNT(DISTINCT t1.product_id) as widget_order,
  ARRAY_AGG(DISTINCT t1.product_id) as widget_order_prod_list,
  SUM(quantity) as widget_quantity,
  SUM(quantity*price) as widget_revenue,
  FROM get_widget_orders t1
  INNER JOIN get_raw_orders t2
  ON (t1.visitor_id=t2.visitor_id and t1.day=t2.day and t1.product_id=t2.product_id)
  INNER JOIN get_clicks_raw t3 
  ON (t1.visitor_id=t3.visitor_id and t1.day=t3.day and t1.product_id=t3.raw_product_id)
  GROUP BY 1,2
)

SELECT 
t1.os_type,
t1.visitor_id, 
t1.day,
widget_impression,
widget_impression_prod_list,
widget_click,
widget_click_prod_list,
widget_cartadd,
widget_cartadd_prod_list,
widget_order,
widget_order_prod_list,
widget_quantity,
widget_revenue
FROM get_impressions t1
LEFT JOIN get_clicks t2 
ON (t1.visitor_id=t2.visitor_id AND t1.day=t2.day)
LEFT JOIN get_cartadds t3 
ON (t1.visitor_id=t3.visitor_id AND t1.day=t3.day)
LEFT JOIN get_orders t4
ON (t1.visitor_id=t4.visitor_id AND t1.day=t4.day)
