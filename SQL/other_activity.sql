DECLARE start_date STRING DEFAULT '2022-04-19';
DECLARE end_date STRING DEFAULT '2022-05-08';
-- DECLARE tenet STRING DEFAULT "facl";

DROP TABLE IF EXISTS `core-search-dev.sandbox_shweta.other_activity`;
CREATE TABLE IF NOT EXISTS `core-search-dev.sandbox_shweta.other_activity` AS
with slp_visits as(
    SELECT 
    visitor_id, 
    day, 
    COALESCE(t2.product_id, t1.variant_id) as product_id,
    FROM (
        SELECT 
        CONCAT(post_visid_high, post_visid_low) as visitor_id,
        day,
        regexp_replace(JSON_EXTRACT_SCALAR(click_data1, '$.product_id'), ',', '') as variant_id
        FROM `falabella-search-prod.relevancy_matrices.facl_slp_session_data`,
        UNNEST(click_data) as click_data1
        WHERE day BETWEEN DATE(start_date) AND DATE(end_date) 
    )t1
    LEFT JOIN `falabella-search-prod.relevancy_matrices.facl_product_variant_mapping` t2
    ON (t1.variant_id=t2.variant_id)
    WHERE t1.variant_id not in ("", "na") and t1.variant_id IS NOT NULL
),

slp_visits_agg as(
SELECT 
visitor_id,
day,
COUNT(DISTINCT product_id) as slp_visit_product_click_count,
ARRAY_AGG(DISTINCT product_id IGNORE NULLS) as slp_visit_product_click_list
FROM slp_visits
GROUP BY 1,2
),

plp_visits as(
    SELECT 
    visitor_id, 
    day, 
    COALESCE(t2.product_id, t1.variant_id) as product_id,
    FROM (
        SELECT 
        CONCAT(post_visid_high, post_visid_low) as visitor_id,
        day,
        regexp_replace(JSON_EXTRACT_SCALAR(click_data1, '$.product_id'), ',', '') as variant_id
        FROM `falabella-search-prod.relevancy_matrices.facl_plp_session_data`,
        UNNEST(click_data) as click_data1
        WHERE day BETWEEN DATE(start_date) AND DATE(end_date) 
    )t1
    LEFT JOIN `falabella-search-prod.relevancy_matrices.facl_product_variant_mapping` t2
    ON (t1.variant_id=t2.variant_id)
    WHERE t1.variant_id not in ("", "na") and t1.variant_id IS NOT NULL
),

plp_visits_agg as(
SELECT 
visitor_id,
day,
COUNT(DISTINCT product_id) as plp_visit_product_click_count,
ARRAY_AGG(DISTINCT product_id IGNORE NULLS) as plp_visit_product_click_list
FROM plp_visits
GROUP BY 1,2
ORDER BY 1,2
),

session_visits as(
    SELECT 
    visitor_id, 
    day, 
    COALESCE(t2.product_id, t1.variant_id) as product_id,
    FROM (
        SELECT 
        CONCAT(post_visid_high, post_visid_low) as visitor_id,
        day,
        regexp_replace(JSON_EXTRACT_SCALAR(click_data1, '$.product_id'), ',', '') as variant_id
        FROM `falabella-search-prod.relevancy_matrices.facl_session_data`,
        UNNEST(click_data) as click_data1
        WHERE day BETWEEN DATE(start_date) AND DATE(end_date) 
    )t1
    LEFT JOIN `falabella-search-prod.relevancy_matrices.facl_product_variant_mapping` t2
    ON (t1.variant_id=t2.variant_id)
    WHERE t1.variant_id not in ("", "na") and t1.variant_id IS NOT NULL
),

session_visits_agg as(
SELECT 
visitor_id,
day,
COUNT(DISTINCT product_id) as session_visit_product_click_count,
ARRAY_AGG(DISTINCT product_id IGNORE NULLS) as session_visit_product_click_list
FROM session_visits
GROUP BY 1,2
ORDER BY 1,2
)

-- add home page visits
SELECT 
COALESCE(COALESCE(t1.visitor_id, t2.visitor_id), t3.visitor_id) visitor_id, 
COALESCE(COALESCE(t1.day, t2.day), t3.day) day, 
session_visit_product_click_count, session_visit_product_click_list,
plp_visit_product_click_count, plp_visit_product_click_list,
slp_visit_product_click_count, slp_visit_product_click_list
FROM session_visits_agg t1
FULL OUTER JOIN plp_visits_agg t2 ON (t1.visitor_id=t2.visitor_id AND t1.day=t2.day)
FULL OUTER JOIN slp_visits_agg t3 ON (t1.visitor_id=t3.visitor_id AND t1.day=t3.day)
