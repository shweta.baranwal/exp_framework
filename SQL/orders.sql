DECLARE start_date STRING DEFAULT '2022-04-19';
DECLARE end_date STRING DEFAULT '2022-05-08';

DROP TABLE IF EXISTS `core-search-dev.sandbox_shweta.orders`;
CREATE TABLE IF NOT EXISTS `core-search-dev.sandbox_shweta.orders` AS 
WITH get_orders as(
  SELECT  
  CONCAT(post_visid_high, post_visid_low) as visitor_id,
  day,
  t1.product_id as variant_id,
  COALESCE(t2.product_id, t1.product_id) as product_id,
  quantity,
  price
  FROM `falabella-search-prod.recommendation.facl_omniture_orders` t1
  LEFT JOIN `falabella-search-prod.relevancy_matrices.facl_product_variant_mapping` t2
  ON (t1.product_id=t2.variant_id)
  WHERE day BETWEEN DATE(start_date) and DATE(end_date)
)

SELECT  
visitor_id,
day,
COUNT(DISTINCT product_id) as total_order,
ARRAY_AGG(DISTINCT product_id IGNORE NULLS) as total_order_prod_list,
SUM(quantity) as total_quantity,
SUM(quantity*price) as total_revenue
FROM get_orders
GROUP BY 1,2