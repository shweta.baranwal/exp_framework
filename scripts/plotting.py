import numpy as np
import pandas as pd
import scipy.stats as stats
from experiment_analysis.ttest import summarize_ttest
from experiment_analysis.ratio_metric import ttest_ratio_metric
from experiment_analysis.capping import apply_capping
from plotly.offline import download_plotlyjs, init_notebook_mode, iplot
import plotly.graph_objs as go


def get_run_date_result(
    df, 
    _date, 
    metric,
    params
):
    """Calculates t-test of a given metric for a particular date
    :param df: pd.Dataframe
    :param _date: str date for which t-test is required
    :param metric: str name/column of dataframe for which t-test is required
    :param params: dict dictionary of parameters
    :return: dictionary of t-test result
    :rtype: dict
    """
    # Keep subjects where bucketing event is before current date
    # Set events to zero where date of event (e.g., brand order created) is after current date
    # Set events to zero where event happened before bucketing as a safeguard. Should be done in main query.
    idx = df["bucketed_at"] <= _date
    df_date = df.loc[idx].reset_index(drop=True)
    df_date[metric].loc[df_date["event_date"] > _date] = 0
    df_date[metric].loc[df_date["event_date"] < df_date["bucketed_at"]] = 0

    # Must aggregate df to level of unit of randomization for correct t-test
    df_agg_metric = (df_date[["visitor_id", "bucket_name", metric]]
                        .groupby(["visitor_id", "bucket_name"])
                        .agg('sum')
                        .reset_index())

    merge_cols = ["visitor_id", "bucket_name"]

    #capping metric for outliers
    params.update({"metric": f"{metric}_capped"})
    df_agg_metric[f"{metric}_capped"] = apply_capping(
        df_agg_metric, metric)

    # Calculate ttest results for desired metric

    row = summarize_ttest(df_agg_metric, **params)
    # row["ds"] = _date - np.timedelta64(1, 'D')
    row["ds"] = _date 
    row["bucket_count"] = df_agg_metric.shape[0]
    row["metric"] = metric
    
    return row



def get_timeseries(
    df,
    metric,
    control_name="control",
    treatment_name="treatment",
    confidence_level=0.9,
):
    """Calculates t-test of a given metric during the course of the experiment (uses daterange of df dataframe)
    :param df: pd.Dataframe
    :param metric: str name/column of dataframe for which t-test is required
    :param control_name: str name of control group. Default is control.
    :param treatment_name: str name of treatment group.  Default is treatment.
    :param confidence_level: float confidence interval of the test. Default is 0.9.
    :return: dataframe of t-test
    :rtype: pd.Dataframe
    """
    params = {
        "control_name": control_name,
        "treatment_name": treatment_name,
        "metric": metric,
        "confidence_level": confidence_level
    }
    df.columns = df.columns.str.lower()
    df["bucketed_at"] = pd.to_datetime(df["bucketed_at"])
    df["event_date"] = pd.to_datetime(df["event_date"])
    # df["exp_start_date"] = np.min(df["bucketed_at"])
    df["exp_start_date"] = pd.to_datetime(df["exp_start_date"])
    df["exp_end_date"] = pd.to_datetime(df["exp_end_date"])
    
    run_dates = []
    start_date = df["exp_start_date"].unique()[0]

    while start_date < np.max(df["exp_end_date"]):
        start_date += np.timedelta64(1, 'D')
        run_dates.append(start_date)
    
    rows = []
    for _date in run_dates:
        row = get_run_date_result(df, _date, metric, params)
        rows.append(row)

    return pd.DataFrame(rows)
  



def plot_timeseries(df, datecol="ds", metric="pct_lift",
                    lower="ci_lift_lower", upper="ci_lift_upper", layout=None):

    """plots the t-test result
    :param df: pd.Dataframe: output of get_timeseries definition
    :param datecol: date column. Default is ds. 
    :param metric: metric you want to plot (pct_lift, control_mean,	treatment_mean). Default is pct_lift.
    :param lower: lower bound of the metric. Default is ci_lift_lower.
    :param upper: upper bound of the metric. Default is ci_lift_upper.
    :return: plot 
    :rtype: plot
    """
    init_notebook_mode()

    x = df[datecol].apply(lambda x: f"{x.date()}")
    y = df[metric]
    y_upper = df[upper]
    y_lower = df[lower]

    fig = go.Figure([
        go.Scatter(
            name="% Lift",
            x=x,
            y=y,
            line=dict(color='maroon'),
            mode='lines'
        ),
        go.Scatter(
            name='Upper Bound',
            x=x,
            y=y_upper,
            mode='lines',
            marker=dict(color="#444"),
            line=dict(width=0),
            showlegend=False
        ),
        go.Scatter(
            name='Lower Bound',
            x=x,
            y=y_lower,
            marker=dict(color="#444"),
            line=dict(width=0),
            mode='lines',
            fillcolor='rgba(68, 68, 68, 0.3)',
            fill='tonexty',
            showlegend=False
        )
    ])
    if layout is None:
        layout = {
            "yaxis_title": "lift",
            "hovermode": "x",
            "width": 1000,
            "height": 600
        }
    fig.update_layout(**layout)

    fig.update_layout(
        yaxis={'tickformat': ',.3%'}
    )

    fig.show() 
    
    
    
def get_run_date_result_for_ratio(
    df, 
    _date, 
    numerator,
    denominator,
    visitor_level,
    params
):

    """Calculates t-test of a ratio metric for a particular date
    :param df: pd.Dataframe
    :param _date: str date for which t-test is required
    :param numerator: str Name of dataframe column containing metric numerator
    :param denominator: str Name of dataframe column containing metric denominator
    :param visitor_level: true if the ratio metric is count of users with non-zero metric else false
    :param params: dict dictionary of parameters
    :return: dictionary of t-test result
    :rtype: dict
    """
    # Keep subjects where bucketing event is bfore current date
    # Set events to zero where date of event (e.g., brand order created) is after current date
    # Set events to zero where event happened before bucketing as a safeguard. Should be done in main query.
    idx = df["bucketed_at"] <= _date
    df_date = df.loc[idx].reset_index(drop=True)
    df_date[numerator].loc[df_date["event_date"] > _date] = 0
    df_date[numerator].loc[df_date["event_date"] < df_date["bucketed_at"]] = 0
    df_date[denominator].loc[df_date["event_date"] > _date] = 0
    df_date[denominator].loc[df_date["event_date"] < df_date["bucketed_at"]] = 0


    # Must aggregate df to level of unit of randomization for correct t-test
    df_agg_metric_num = (df_date[["visitor_id", "bucket_name", numerator]]
                        .groupby(["visitor_id", "bucket_name"])
                        .agg('sum')
                        .reset_index())
    df_agg_metric_denom = (df_date[["visitor_id", "bucket_name", denominator]]
                        .groupby(["visitor_id", "bucket_name"])
                        .agg('sum')
                        .reset_index())

    if visitor_level:
        df_agg_metric_num[numerator]=np.where(df_agg_metric_num[numerator] > 0, 1, 0)
        df_agg_metric_denom[denominator]=np.where(df_agg_metric_denom[denominator] > 0, 1, 0)

    #capping metric for outliers
    params.update({"numerator": f"{numerator}_capped"})
    df_agg_metric_num[f"{numerator}_capped"] = apply_capping(
        df_agg_metric_num, numerator)

    params.update({"denominator": f"{denominator}_capped"})
    df_agg_metric_denom[f"{denominator}_capped"] = apply_capping(
        df_agg_metric_denom, denominator)

    merge_cols = ["visitor_id", "bucket_name"]
    df_agg_joined = df_agg_metric_denom.merge(
        df_agg_metric_num, on=merge_cols, how="left")
    df_agg_joined[f"{numerator}_capped"].fillna(0, inplace=True)


    # Calculate ttest results for desired metric

    row = ttest_ratio_metric(df_agg_joined, **params)
    row["ds"] = _date 
    # row["ds"] = _date - np.timedelta64(1, 'D')
    row["bucket_count"] = df_agg_joined.shape[0]
    row["metric"] = numerator + '/' + denominator

    return row
    
    

def get_timeseries_for_ratio(
    df,
    numerator,
    denominator,
    visitor_level=True,
    control_name="control",
    treatment_name="treatment",
):
    """Calculates t-test of a given metric during the course of the experiment (uses daterange of df dataframe)
    :param df: pd.Dataframe
    :param numerator: str Name of dataframe column containing metric numerator
    :param denominator: str Name of dataframe column containing metric denominator
    :param visitor_level: true if the ratio metric is count of users with non-zero metric else false
    :param control_name: str name of control group. Default is control.
    :param treatment_name: str name of treatment group.  Default is treatment.
    :return: dataframe of t-test
    :rtype: pd.Dataframe
    """
    params = {
        "numerator": numerator,
        "denominator": denominator,
        "bucket_col": "bucket_name",
        "treatment_label": treatment_name,
        "control_label": control_name
    }
    
    
    df.columns = df.columns.str.lower()
    df["bucketed_at"] = pd.to_datetime(df["bucketed_at"])
    df["event_date"] = pd.to_datetime(df["event_date"])
    # df["exp_start_date"] = np.min(df["bucketed_at"])
    df["exp_start_date"] = pd.to_datetime(df["exp_start_date"])
    df["exp_end_date"] = pd.to_datetime(df["exp_end_date"])
    
    run_dates = []
    start_date = df["exp_start_date"].unique()[0]

    while start_date < np.max(df["event_date"]):
        start_date += np.timedelta64(1, 'D')
        run_dates.append(start_date)
    
    rows = []
    for _date in run_dates:
        row = get_run_date_result_for_ratio(df, _date, numerator, denominator, visitor_level, params)
        rows.append(row)

    return pd.DataFrame(rows)
    