import numpy as np
import pandas as pd
import datetime
import plotly.express as px

from experiment_analysis.capping import apply_capping
from experiment_analysis.exceptions import (
    InvalidInputDataframe,
    InvalidParameter,
)


#for a particular metric get bucket aggregations
def get_bucket_summary_for_metric(df_summary, metric, summary_date):
    """Calculates metrics info for all buckets
    :param df: pd.Dataframe 
    :param metric: str Name of dataframe column containing experiment metric to summarise
    :param summary_date: str Date of summary
    :return: list of dictionary for metric info for all buckets
    :rtype: list
    """
    rows = []
      
    for bucket in df_summary.bucket_name.unique():
        df_summary_bucket = df_summary[df_summary['bucket_name']==bucket]
        df_summary_bucket[f"{metric}_bool"] = np.where(df_summary_bucket[metric]>0, 1, 0)
        
        row = {
            "bucket_name": bucket,
            "metric": metric,
            "sum": df_summary_bucket[metric].sum(),
            "mean": df_summary_bucket[metric].mean(),
            "visitors_with_metric": df_summary_bucket[f"{metric}_bool"].sum(),
            "total_visitors": df_summary_bucket[metric].count(),
            "visitor_ratio": df_summary_bucket[f"{metric}_bool"].sum()/df_summary_bucket[metric].count(), 
            "summary_date": summary_date
        }

        rows.append(row)
    return rows



def get_summary(df, summary_date, filters_=None, metric_list=None, capping=False):
    """Calculates metrics info for all buckets 
    :param df: pd.Dataframe 
    :param summary_date: str Date of summary
    :param filters_: dict Dictionary of filters {col_name: value}
    :param metric_list: list of metric
    :param capping: bool if capping needed in summary
    :return: dataframe of summary
    :rtype: pd.DataFrame
    """

    if not isinstance(summary_date, datetime.date):
        summary_date = pd.to_datetime(summary_date)
    
    if not metric_list:
        metric_list = [col for col in df.columns if df.dtypes[col] not in ('object', 'datetime64[ns]')]
    else:
        if not isinstance(metric_list, list):
            raise InvalidParameter("metric_list must be list of column")
    
    if filters_:
        if not isinstance(filters_, dict):
            raise InvalidParameter("filters_ must be dictionary {col_name: value}")
        for col, value in filters_.items():
            print(f"Applying filter {col}={value}")
            df = df[df[col]==value]
    
    # removing dates that are after summary date and bucketed date
    idx = df["bucketed_at"] <= summary_date
    df = df.loc[idx].reset_index(drop=True)
    df = df[["visitor_id", "bucket_name", "event_date", "bucketed_at", *metric_list]]
    df = df[df["event_date"] <= summary_date]
    df = df[df["event_date"] >= df["bucketed_at"]]
    
    summary = []
    for metric in metric_list:
        # Must aggregate df to level of unit of randomization for correct t-test
        df_agg_metric = (df[["visitor_id", "bucket_name", metric]]
                         .groupby(["visitor_id", "bucket_name"])
                         .agg('sum')
                         .reset_index())
        if capping:
            df_agg_metric[metric] = apply_capping(df_agg_metric, metric)
        row = get_bucket_summary_for_metric(df_agg_metric, metric, str(summary_date.date()))
        summary.extend(row)     
    return pd.DataFrame(summary)



def get_summary_timeseries(df, metric, filters_=None, capping=False):
    """Calculates metrics info for all buckets 
    :param df: pd.Dataframe 
    :param metric: str, metric for which summary is required
    :param filters_: dict Dictionary of filters {col_name: value}
    :param capping: bool if capping needed in summary
    :return: dataframe of summary
    :rtype: pd.DataFrame
    """

    if metric not in df.columns:
        raise InvalidInputDataframe(
            f"input dataframe is missing metric column {metric!r}"
        )
        
    if filters_:
        if not isinstance(filters_, dict):
            raise InvalidParameter("filters_ must be dictionary {col_name: value}")
        for col, value in filters_.items():
            print(f"Applying filter {col}={value}")
            df = df[df[col]==value]


    df["bucketed_at"] = pd.to_datetime(df["bucketed_at"])
    df["event_date"] = pd.to_datetime(df["event_date"])
    # df["exp_start_date"] = np.min(df["bucketed_at"])
    df["exp_start_date"] = pd.to_datetime(df["exp_start_date"])
    df["exp_end_date"] = pd.to_datetime(df["exp_end_date"])
    
    run_dates = []
    start_date = df["exp_start_date"].unique()[0]

    while start_date < np.max(df["exp_end_date"]):
        start_date += np.timedelta64(1, 'D')
        run_dates.append(start_date)
    
    rows = []
    for _date in run_dates:
        idx = df["bucketed_at"] <= _date
        df_date = df.loc[idx].reset_index(drop=True)
        df_date[metric].loc[df_date["event_date"] > _date] = 0
        df_date[metric].loc[df_date["event_date"] < df_date["bucketed_at"]] = 0

        # Must aggregate df to level of unit of randomization for correct t-test
        df_agg_metric = (df_date[["visitor_id", "bucket_name", metric]]
                         .groupby(["visitor_id", "bucket_name"])
                         .agg('sum')
                         .reset_index())
        if capping:
            df_agg_metric[metric] = apply_capping(df_agg_metric, metric)
        row = get_bucket_summary_for_metric(df_agg_metric, metric, str(pd.to_datetime(_date).date()))       
        rows.extend(row)

    return pd.DataFrame(rows)


def plot_ts_summary(df, y, x='summary_date', color='bucket_name'):
    y=y
    metric=df['metric'].unique()[0]
    fig = px.line(df, x, y, color)
    fig.show()
    
    