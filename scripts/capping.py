import numpy as np
import pandas as pd

from experiment_analysis.exceptions import (
    InvalidInputDataframe,
    InvalidParameter,
)


def apply_capping(
    df: pd.DataFrame,
    metric: str,
    sd_threshold: int = 5,
) -> pd.Series:
    """Calculates metrics capped at threshold of given SDs from mean (upper bound capping only). Intended for non-negative metrics.
    :param df: pd.Dataframe
    :param metric: str Name of dataframe column containing experiment metric to cap
    :param sd_threshold: sd_threshold Number of standard deviations from mean to set as outlier threshold. Default is 5.
    :return: Capped metric
    :rtype: pd.Series
    """
    
    if metric not in df.columns:
        raise InvalidInputDataframe(
            f"input dataframe is missing metric column {metric!r}"
        )

    if not isinstance(sd_threshold, int):
        raise InvalidParameter(f"sd_threshold must be integer")

    metric_mean = df.loc[df[metric] > 0][metric].mean()
    metric_stdev = df.loc[df[metric] > 0][metric].std()

    upper_bound = metric_mean + sd_threshold * metric_stdev
    capped_metric = pd.Series(
        np.where(df[metric] > upper_bound, upper_bound, df[metric])
    )

    return capped_metric